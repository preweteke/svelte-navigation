import {getActiveWidget, getWidgets, setActiveWidget} from "./store/navigation";

const AXIS = {
    X: 'x',
    Y: 'y'
};

const DIRECTION = {
    LEFT: [AXIS.X, -1],
    RIGHT: [AXIS.X, 1],
    UP: [AXIS.Y, -1],
    DOWN: [AXIS.Y, 1]
};

const ARROW = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40
};

const getEdges = (axis, direction) => {
    let edges;

    if (axis === AXIS.X) {
        if (direction > 0) {
            edges = {near: 'left', far: 'right', first: 'top', second: 'bottom'}
        } else {
            edges = {near: 'right', far: 'left', first: 'top', second: 'bottom'}
        }
    } else if (axis === AXIS.Y) {
        if (direction > 0) {
            edges = {near: 'top', far: 'bottom', first: 'left', second: 'right'};
        } else {
            edges = {near: 'bottom', far: 'top', first: 'left', second: 'right'};
        }
    }

    return edges;
};

class NavigationService {
    init() {
        document.addEventListener('keydown', event => {
            console.log('event key code', event.keyCode);

            let direction;

            switch (event.keyCode) {
                case ARROW.LEFT:
                    direction = DIRECTION.LEFT
                    break;
                case ARROW.RIGHT:
                    direction = DIRECTION.RIGHT
                    break;
                case ARROW.UP:
                    direction = DIRECTION.UP
                    break;
                case ARROW.DOWN:
                    direction = DIRECTION.DOWN
                    break;
            }

            this.moveFocus(direction);
        });
    }

    moveFocus(direction) {
        const widgets = getWidgets();
        const activeWidgetUid = getActiveWidget();
        const [axis, d] = direction;
        const edges = getEdges(axis, d);
        const currentWidget = widgets[activeWidgetUid];

        let target = {
            distance: 0,
            widget: null
        };

        for (const widget of Object.values(widgets)) {
            const currentRect = currentWidget.ref.getBoundingClientRect();
            const currentEdge = currentRect[edges.far];
            const widgetRect = widget.ref.getBoundingClientRect();
            const nearEdge = widgetRect[edges.near];
            const distance = (nearEdge - currentEdge) * d;
            const isInsideIntersection = widgetRect[edges.second] >= currentRect[edges.first];

            if (widget.uid === activeWidgetUid || (nearEdge - currentEdge) * d < 0 || !isInsideIntersection) continue;

            if (!target.widget || distance < target.distance) {
                target = {
                    widget,
                    distance
                }
            }

            setActiveWidget(target.widget?.uid || activeWidgetUid);
        }

    }
}

const Navigation = new NavigationService();

export {Navigation};
