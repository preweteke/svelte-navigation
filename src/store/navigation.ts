import {get, writable} from "svelte/store";

const widgets = writable({});
const activeWidget = writable<string | null>(null);

type WidgetData = {
    uid: string,
    ref: object
}

const addWidget = ({uid, ref}: WidgetData) => {
    widgets.update(currentWidgets => ({...currentWidgets, [uid]: {uid, ref}}));

    activeWidget.set(uid);
};

const getWidgets = () => {
    return get(widgets);
};

const getActiveWidget = () => {
    return get(activeWidget);
};

const setActiveWidget = (uid: string) => {
    activeWidget.set(uid);
};

export {
    widgets,
    activeWidget,
    addWidget,
    getWidgets,
    setActiveWidget,
    getActiveWidget
};
