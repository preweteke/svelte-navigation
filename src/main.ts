import './app.css';
import App from './App.svelte';
import {Navigation} from "./NavigationService";

const app = new App({
  target: document.getElementById('app'),
});

Navigation.init();

export default app;
